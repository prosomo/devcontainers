# GPG TTY
export GPG_TTY=$(tty)

# Make VS Code the default editor
export VISUAL="code --wait"
export EDITOR="$VISUAL"

# Make less the default pager
export PAGER="less"

# Aliases
## IP addresses
alias ip="curl stuff.jakobbouchard.dev/ip.php"

# Prompt
username() {
	echo "%F{magenta}%n%F{white}"
}

in_dir() {
	echo "in %F{green}%~%F{white}"
}

precmd() {
	print -P "\n$(username) $(in_dir)"
}

PROMPT="%F{yellow}λ%F{white} "

